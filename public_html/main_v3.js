var wikishootme = {
        href_target : (L.Browser.mobile ? "" : "_top"),
	sparql_url : 'https://query.wikidata.org/bigdata/namespace/wdq/sparql' ,
	check_reason_no_image : false ,
	zoom_level : 15 ,
	opacity : 0.5 ,
	marker_radius : {
		wikidata : 10 ,
		wikipedia : 5 ,
		commons : 7 ,
		flickr : 6 ,
		mixnmatch : 5 ,
		geojson : 5 ,
		geoshape : 5 ,
		omtiles : 5 ,
		me : 15
	} ,
	thumb_size : 200 ,
	sparql_filter:'',
	language : 'fi' ,
	max_items : 1000 ,
	upload_mode:'upload' ,
	colors : {
		me:'#111111',
		wikidata_image:'#2DC800',
		wikidata_no_image:'#CC3333',
		no_image_copyright:'#aA03FE',
		flickr:'#FF800D',
		wikipedia:'#FFFFAA',
		mixnmatch:'#AE70ED',
		geojson:'#FF70ED',
		geoshape:'#FF70ED',
		rky_lines:'#FF70ED',
		rky_area:'#FF70ED',
		museumroads:'#FF70ED',
		commons:'#62A9FF'
	},
        allowed_geojson_domains:[ 
		'wikimedia.org', 
		'wikidata.org', 
		'wikipedia.org', 
		'wikisource.org', 
		'wikibooks.org', 
		'wikivoyage.org', 
		'wmflabs.org', 
		'wikidocumentaries.io',
                'toolforge.org'
	],

	map_is_set : false ,
	pos : { lat:60.17 , lng:24.9 } ,
	entries : {
		wikidata : {} ,
	} ,
	upload_queue : [] ,
	upload_delay : 100 ,
	worldwide : 1 ,
	json_cache : {} ,

	// Layer stuff
//	show_layers : ['geoshape'] ,
	show_layers : ['rky_area', 'rky_lines'] ,
	overlays : {} ,
	layer_info : {
		key2name : {
			wikidata_image:'Wikidata (with image, 3K max)',
			wikidata_no_image:'Wikidata (no image, 3K max)',
			commons:'Commons images (500 max)',
			wikipedia:'Wikipedia (500 max)',
			mixnmatch:"Mix'n'match",
			geojson:"Geojson",
			geoshape:"Geoshapes",
			rky_area:"RKY areas",
			rky_lines:"RKY lines",
//			museumroads:"Museum and travel roads",
			flickr:'Flickr'

		} ,
		name2key : {}
	} ,
	layers : {
		wikipedia : {} ,
		wikidata_image : {} ,
		wikidata_no_image : {} ,
		commons : {},
                geojson : {},
                geoshape : {},
                rky_area : {},
                rky_lines: {},
//                museumroads: {},
	} ,
	geoshapes : [],

	busy : 0 ,

	escapeHTML : function ( s ) {
		return escattr(s) ;
	} ,

	getHashVars : function () {
		var vars = {} ;
		var hashes = window.location.href.slice(window.location.href.indexOf('#') + 1).split('&');
		$.each ( hashes , function ( i , j ) {
			var hash = j.split('=');
			hash[1] += '' ;
			vars[hash[0]] = decodeURIComponent(hash[1]).replace(/_/g,' ');
		} ) ;
		return vars;
	} ,

	isIframe: function () {
		try {
			return window.self !== window.top;
		} catch (e) {
			return true;
		}
	} ,

	updatePermalink : function () {
		var me = this ;
		var h = [] ;
		h.push ( 'lat='+me.pos.lat ) ;
		h.push ( 'lng='+me.pos.lng ) ;
		h.push ( 'zoom='+me.map.getZoom() ) ;
		//if ( me.language != 'en' ) 
                h.push ( 'interface_language='+me.language ) ;

		var layers = me.show_layers.join(',') ;
		if ( layers != me.full_layers ) h.push ( 'layers='+layers ) ;

		if ( me.sparql_filter == '' ) {
			$('#is_using_filter').hide() ;
		} else {
			h.push ( 'sparql_filter='+encodeURIComponent(me.sparql_filter) ) ;
			$('#is_using_filter').show() ;
		}
		if ( me.worldwide ) h.push ( 'worldwide=1' ) ;
		if ( me.geojson_file ) h.push ( 'geojson=' + encodeURIComponent(me.geojson_file) ) ;
		if ( me.geojson_center ) h.push ( 'geojson_center=1');
		
		wsm_comm.storeCurrentView ( h ) ;
		
		h = '#' + h.join('&') ;
		location.hash = h ;
		me.updateSearchLinks() ;
	} ,
	
	updateSearchLinks : function () {
		var me = this ;
		
		// Get area
		var b = me.map.getBounds() ;
		var ne = b.getNorthEast() ;
		var sw = b.getSouthWest() ;
		
		// Update WD-FIST link
		var sparql = '' ;
		sparql += 'SELECT ?q {' ;
		if ( !me.worldwide ) {
			sparql += ' SERVICE wikibase:box { ?q wdt:P625 ?location . ' ;
			sparql += 'bd:serviceParam wikibase:cornerSouthWest "Point('+sw.lng+' '+sw.lat+')"^^geo:wktLiteral . ' ;
			sparql += 'bd:serviceParam wikibase:cornerNorthEast "Point('+ne.lng+' '+ne.lat+')"^^geo:wktLiteral }' ;
		}
		sparql += ' ' + me.sparql_filter + ' }' ;
		var url = "https://tools.wmflabs.org/fist/wdfist/index.html?sparql=" ;
		url += encodeURIComponent ( sparql ) ;
		url += '&no_images_only=1&search_by_coordinates=1&remove_multiple=1&prefilled=1' ;
		$('#wdfist').show().attr ( { href : url } ) ;
		
		var distance = ne.distanceTo(sw) ;
		var radius_km = Math.round ( (distance/2) / 1000 ) ;
		if ( radius_km <= 0 ) radius_km = 1 ;
		var center = me.map.getCenter() ;
		url = "https://tools.wmflabs.org/fist/check_flickr_geo.php?lat="+center.lat+"&lon="+center.lng+"&radius="+radius_km+"&doit=1" ;
		$('#flickr').show().attr ( { href : url } ) ;
	} ,

	setBusy : function ( d ) {
		var me = this ;
		me.busy += d ;
		console.log("busy: " + me.busy); 
		if ( me.busy == 0 ) { // All done
			$('#busy').hide() ;
			try { me.layers.wikipedia.bringToFront() ; } catch ( e ) { }
			me.updatePermalink() ;
		} else if ( d == 1 && me.busy == 1 ) { // Starting to be busy...
			$('#busy').show() ;
		}
	} ,
	
	cleanLayers : function () {
		var me = this ;
		$.each ( me.layers , function ( k , v ) {
                        if (k=='rky_area') {
                            return;
                        }
                        if (k=='rky_lines') {
                            return;
                        }
                        if (k=='museumroads') {
                            return;
                        }
			v.clearLayers() ;
		} ) ;
		$.each ( me.entries , function ( mode , entries ) {
			me.entries[mode] = {} ;
		} ) ;
	} ,
	/* Popupin kuva */
	createImageThumbnail : function ( image ) {
		var me = this ;
		var url = 'https://commons.wikimedia.org/wiki/Special:Redirect/file/'+escattr(encodeURIComponent(image))+'?width='+me.thumb_size+'px&height='+me.thumb_size+'px' ;
		var h = '' ;
		h += "<div class='thumb'>" ;
		h += "<a target='" + me.href_target + "' href='https://commons.wikimedia.org/wiki/File:" + escattr(encodeURIComponent(image)) + "'>" ;
		h += "<img src='" + url + "' border=0 width='"+me.thumb_size+"px' height='"+me.thumb_size+"px' alt='Loading image...' />" ;
		h += "</a></div>" ;
		h += "<div class='smallimgname'>" + me.escapeHTML(image) + "<br /></div>" ;
		return h ;
	} ,
	
	createItemFromImage : function ( a ) {
		var me = wikishootme ;
		var image = $(a).attr ( 'image' ) ;
		var image_pos = { lat:$(a).attr('lat') , lng:$(a).attr('lng') } ;

		me.createNewItem ( {
			pos:image_pos,
			label_default:image.replace(/\.[^.]+$/,'').replace(/ - geograph.org.uk.*$/,''),
			image:image
		} ) ;

		return false ;
	} ,
	
	createPopup : function ( entry ) {
		var me = this ;
		var h = '' ;
		h += "<div>" ;
		h += "<div style='text-align:center'>" ;
                if (typeof entry.sitelink != "undefined" )  h += "<div><a target='" + me.href_target  +"' href='" + escattr(entry.sitelink) + "' ><b>" + me.escapeHTML(entry.label) + "</b></a></div>" ;
                else  h += "<div><b>" + me.escapeHTML(entry.label) + "</b></div>" ;
		if ( typeof entry.description != 'undefined' ) h += "<div>" + me.escapeHTML(entry.description) + "</div>" ;
		if ( typeof entry.note != 'undefined' ) h += "<div><i>" + me.escapeHTML(entry.note) + "</i></div>" ;

                 external_links=''
                p4009_url='http://www.rky.fi/read/asp/r_kohde_det.aspx?KOHDE_ID=';
//              p5513_url='';
                p5310_url='https://www.kyppi.fi/palveluikkuna/rapea/read/asp/r_kohde_det.aspx?KOHDE_ID=';
                p4106_url='https://www.kyppi.fi/to.aspx?id=112.';
                p8355_url='https://fiwiki-tools-web.toolforge.org/helsinki_city_map_redirect.php?P8355=';
                var rakennusperintorekisteri_labels = { fi: 'Rakennusperintorekisteri', en: 'Protected Buildings Register', sv:'Byggnadsarvsregistret' };
                var muinaisjaannosrekisteri_labels = { fi: 'Muinaisjäännösrekisteri', en: 'Antiquities record', sv:'Fornminnesregister' };
                var rakennustunnus_labels = { fi: 'Rakennustunnus', en: 'Building id', sv:'Building id' };

                if (typeof rakennusperintorekisteri_labels[me.language] != "undefined") rakennusperintorekisteri_label = rakennusperintorekisteri_labels[me.language];
                else rakennusperintorekisteri_label = 'Protected Buildings Register';

                if (typeof muinaisjaannosrekisteri_labels[me.language] != "undefined") muinaisjaannosrekisteri_label = muinaisjaannosrekisteri_labels[me.language];
                else muinaisjaannosrekisteri_label =  'Antiquities record';

                if (typeof rakennustunnus_labels[me.language] != "undefined") rakennustunnus_label = rakennustunnus_labels[me.language];
                else rakennustunnus_label = 'Building id';

                if ( typeof entry.p4009 != "undefined"  ) { external_links += "<li>RKY: <a target='" + me.href_target +"' href='" + p4009_url + entry.p4009.value + "'>" +  entry.p4009.value + "</a></li>"; }
//              if ( typeof entry.p5313 != "undefined"  ) { external_links += "<li><a href='" + p5513_url + entry.p5313.value + ">" +  entry.p5313.value + "</a></li>"; }
                if ( typeof entry.p5310 != "undefined"  ) { external_links += "<li>"+ rakennusperintorekisteri_label +": <a target='" + me.href_target +"' href='" + p5310_url + entry.p5310.value + "'>" +  entry.p5310.value + "</a></li>"; }
                if ( typeof entry.p4106 != "undefined"  ) { external_links += "<li>"+ muinaisjaannosrekisteri_label +": <a target='" + me.href_target +"' href='" + p4106_url + entry.p4106.value + "'>" +  entry.p4106.value + "</a></li>"; }
                if ( typeof entry.p8355 != "undefined"  ) { external_links += "<li>"+ rakennustunnus_label +": <a target='" + me.href_target +"' href='" + p8355_url + entry.p8355.value + "'>" +  entry.p8355.value + "</a></li>"; }
                external_links += "<li>Wikidata: <a target='" + me.href_target  +"' href='" + escattr(entry.url) + "' >" + me.escapeHTML(entry.page) + "</a></li>"		

                if ( external_links!='') h+="<ul style='text-align:left'>" + external_links + "</ul>";
		if ( typeof entry.mixnmatch != 'undefined' ) {
			if ( entry.mixnmatch.q != null ) {
				var q = entry.mixnmatch.q ;
				var qlink = "<a target= '" + me.href_target +"' href='https://www.wikidata.org/wiki/Q" + q + "' target='_blank'>Q" + q + "</a>" ;
				if ( entry.mixnmatch.user == 0 ) h += "<div><i>Preliminarily</i> matched to " + qlink + "</div>" ;
				else h += "<div>Matched to " + qlink + "</div>" ;
			} else {
				h += "<div><i>Not matched to Wikidata</i></div>" ;
			}
		}
		
		if ( entry.mode == 'flickr' ) {
			h += "<div class='popup_section' style='text-align:center'>" ;
			h += "<a target='" + me.href_target +"' href='"+entry.url+"' target='_blank'><img src='" + entry.thumburl + "' border=0 style='max-width:100%' /></a>" ;
			h += "</div>" ;
			h += "<div flickr_id='"+entry.flickr_id+"' class='popup_section transfer2flickr'></div>" ;
		} else if ( entry.mode == 'mixnmatch' ) {
			h += "<div class='popup_section' style='text-align:center'>" ;
			h += "MnM1" ;
//			h += "<a href='"+entry.url+"' target='_blank'><img src='" + entry.thumburl + "' border=0 style='max-width:100%' /></a>" ;
			h += "</div>" ;
//			h += "<div flickr_id='"+entry.flickr_id+"' class='popup_section transfer2flickr'></div>" ;
		} else if ( typeof entry.image != 'undefined' ) {
			h += me.createImageThumbnail ( entry.image ) ;
			h += me.createUploadForm(entry,  me.tt.t('upload_file'));

                        if (me.language=="fi") {
				h += me.createErrorForm(entry, "Ehdota korjausta");
                        } else if (me.language=="sv") {
				h += me.createErrorForm(entry, "Föreslå fix");
                        } else if (me.language=="es") {
				h += me.createErrorForm(entry, "Sugerir una mejora");
                        } else {
				h += me.createErrorForm(entry, "Suggest fix");
                        }

			
/*			if ( entry.mode == 'commons' ) {
				h += "<div class='popup_section'>" ;
				h += "<a href='#' image='"+escattr(entry.image)+"' lat='"+entry.pos[0]+"' lng='"+entry.pos[1]+"' class='create_item_from_image' onclick='return wikishootme.createItemFromImage($(this));return false'>" + me.tt.t('create_wd_from_image') + "</a>" ;
				h += "</div>" ;
			}
*/
			
		} else if ( entry.mode == 'wikidata' ) { // Wikidata, no image

			h += me.createUploadForm(entry,  me.tt.t('upload_file'));
//                        console.log(me.tt.t('upload_file'));

                        if (me.language=="fi") {
				h += me.createErrorForm(entry, "Ehdota korjausta");
                        } else if (me.language=="sv") {
				h += me.createErrorForm(entry, "Föreslå fix");
                        } else if (me.language=="es") {
				h += me.createErrorForm(entry, "Sugerir una mejora");
                        } else {
				h += me.createErrorForm(entry, "Suggest fix");
                        }

                       


		} else if ( entry.mode == 'wikipedia' && typeof entry.server != 'undefined' ) {
			h += "<div class='pageimage_toload' server='" + entry.server + "' page='" + escattr(entry.page) + "'></div>" ;
		}
		
		if ( typeof entry.commonscat != 'undefined' ) {
			h += "<div class='popup_section'>" ;
                        h += "<a target='" + me.href_target +"' href='//commons.wikimedia.org/wiki/Category:"+escattr(encodeURIComponent(entry.commonscat.replace(/ /g,'_')))+"'  title='"+escattr(''+me.tt.t('commons_category'))+"'>"+me.escapeHTML(entry.commonscat)+"</a>" ;
			h += "<img src='https://upload.wikimedia.org/wikipedia/commons/4/4a/Commons-logo.svg' style='height:0.9em; padding-left:0.25em' />";
			h += "</div>" ;
		}
		
		h += "<div class='popup_coords'><span class='coordinates'>" + entry.pos[0] + ", " + entry.pos[1] + "</span>" ;
		h += " <a style='user-select:none' target='" + me.href_target +"' href='http://www.instantstreetview.com/@"+entry.pos[0]+","+entry.pos[1]+",0h,0p,1z' tt_title='streetview' >&#127968;</a>" ;
		if ( wsm_comm.isLoggedIn() ) {
			h += " [<a href='#' style='user-select:none' onclick='wikishootme.editCoordinates(this,\""+entry.page+"\","+entry.pos[0]+","+entry.pos[1]+");return false' title='edit coordinates'>e</a>]" ;
		}
		h += "</div>" ;
		
		if ( typeof me.marker_me != 'undefined' ) {
			var pos = me.marker_me.getLatLng() ;
			h += "<div style='font-size:10pt'>" ;
			h += "<a target='" + me.href_target +"' href='https://maps.google.co.uk/maps?dirflg=w&saddr="+pos.lat+","+pos.lng+"&daddr="+entry.pos[0]+","+entry.pos[1]+"'>"+me.tt.t('route')+"</a>" ;
			h += "</div>" ;
		}
		
		h += "</div>" ; // center
		h += "</div>" ;
		
		var ret = L.popup().setContent ( h ) ;
		return ret ;
	} ,
	createErrorForm(entry, error_text) {
		var h="";
		var me=this;
                var title="Korjausehdotus kohteelle " + entry.label ;
                var summary="Korjausehdotus kohteelle " +entry.label +". [[:d:" + entry.page +"]]";
                var url="https://commons.wikimedia.org/w/index.php?";
		url+="title=Commons:Wiki_Loves_Monuments_in_Finland/korjaukset";
		url+="&action=edit";
		url+="&section=new";
		url+="&preload=Commons:Wiki_Loves_Monuments_in_Finland/korjaukset/pohja";
		url+="&editintro=Commons:Wiki_Loves_Monuments_in_Finland/korjaukset/ohje";
		url+="&preloadtitle=" + encodeURIComponent(title);
		url+="&summary=" +  encodeURIComponent(summary);
                url+="&preloadparams%5b%5d=" + encodeURIComponent(entry.page)
		url+="&preloadparams%5b%5d=" + encodeURIComponent(entry.pos[0])
		url+="&preloadparams%5b%5d=" + encodeURIComponent(entry.pos[1])

		h += "<div style='margin-top:15px'>" ;
		h += "<a target='" + me.href_target +"' href='" + url + "'>" + error_text +"</a>";
		h += "</div>";
                return h;
	
	},
	createUploadForm(entry, upload_text) {
		var h="";
		var me=this;
		var desc = "{{Information\n|Description=[[d:" + entry.page + "|" + entry.label + "]]\n|Source=self-made\n|Date=\n|Author=[[User:"+wsm_comm.userinfo.name+"|]]\n|Permission=\n|other_versions=\n}}\n" ;
		desc += "{{Object location|"+entry.pos[0]+"|"+entry.pos[1]+"}}\n<!--LOC-->\n\n" ;
		desc += "=={{int:license-header}}==\n{{self|cc-by-sa-3.0}}" ;
		var url="https://commons.wikimedia.org/w/index.php?title=Special:UploadWizard&campaign=wlm-fi-rephotography-wikidata&id="+ entry.page +"&descriptionlang=fi&description="+ encodeURIComponent(entry.label) + "&lat=" + entry.pos[0]+ "&lon=" + entry.pos[1] +"&categories=Cultural_heritage_monuments_in_Finland";
		if (entry.commonscat) {
			url+="|" + encodeURIComponent(entry.commonscat.replace(/ /g,'_'));
		}

		h += "<div style='margin-top:15px'>" ;
		h += "<a target='" + me.href_target +"' href='" + url + "'><button class='btn btn-primary btn-file'>" + upload_text +"</button></a>";

/*		h += "<!--form method='post' enctype='multipart/form-data' action='"+wsm_comm.api_v3+"' class='form form-inline' target='_blank'>" ;
		h += '<label class="btn btn-primary btn-file"><!--' + me.tt.t('upload_file')  + '-->'+ upload_text + ' <input name="file" type="file" accept="image/*;capture=camera" onchange="wikishootme.uploadFileHandler(this)" style="display: none;"></label>' ;
		h += "<input type='hidden' name='action' value='"+me.upload_mode+"' />" ;
		h += "<input type='hidden' name='q' value='"+entry.page+"' />" ;
		h += "<input type='hidden' name='wpDestFile' value='" + escattr(entry.label) + ".jpg' />" ;
		h += "<input type='hidden' name='wpUploadDescription' value='" + escattr(desc) + "' />" ;
		h += "<input type='submit' style='display:none' name='wpUpload' value='" + me.tt.t('upload_file') + "' />" ;
		h += "</form-->" ;*/
		h += "</div>" ;
/*				
		h += "<div class='add_image2item'>" ;
		h += "<form class='form form-inline' onSubmit='return wikishootme.addImageToItemHandler($(this))'>" ;
		h += "<input type='hidden' name='q' value='"+escattr(entry.page)+"' />" ;
		h += "<input type='text' name='filename' placeholder='"+escattr(me.tt.t('commons_file_name'))+"' />" ;
		h += "<input type='submit' value='"+escattr(me.tt.t('add2item'))+"' />" ;
		h += "</div>" ;
*/
		return h;
	},

	editCoordinates : function ( a , q , lat , lon ) {
		var me = wikishootme ;
		var ret = prompt ( "Edit coordinates" , lat+'/'+lon ) ;
		if ( ret == null ) return false ; // Cancel
		if ( !ret.match ( /^\s*[0-9\.]+\s*\/\s*[0-9\.]+\s*$/ ) ) {
			alert ( "Bad format, not changing coordinates" ) ;
			return false ;
		}
		if ( ret == lat+'/'+lon ) {
			alert ( "New coordinates are the same as the old ones, not changed" ) ;
			return false ;
		}
		
		wsm_comm.getWSM ( {
			action:'changeCoordinates',
			coordinates:ret,
			q:q
		} , function ( d ) {
			if ( d.status != 'OK' ) {
				alert ( "ERROR: " + d.status ) ;
			} else {
				var cs = $($($(a).parents('div.popup_coords').get(0)).find('span.coordinates')) ;
				cs.text ( ret ) ;
			}
		} ) ;
		return false ;
	} ,

	uploadFileHandler : function ( o ) {
		var me = wikishootme ;
		var form = $(o).parents('form').get(0) ;
		
		// Upload as separate tab; fallback for older browsers
		if ( me.upload_mode == 'upload' ) {
			$(form).submit() ;
			return false ;
		}
		
		// Upload in background
		$($(form).parent()).append ( me.tt.t('uploading') ) ;
		var o = {
			data : new FormData($(form)[0]),
			is_uploading:false,
			is_uploaded:false
		}
		me.upload_queue.push ( o ) ;
		me.uploadNext() ;
	} ,
	
	clearUploads : function () {
		var me = this ;
		me.upload_queue = [] ;
		$('#dropdownUploadsLi').hide() ;
		return false ;
	} ,
	
	showUploadStatus : function () {
		var me = this ;
		
		var cnt_uploaded = 0 ;
		$.each ( me.upload_queue , function ( k , v ) {
			if ( v.is_uploaded ) cnt_uploaded++ ;
		} ) ;
		
		var h = '' ;
		if ( cnt_uploaded == me.upload_queue.length ) {
			h += "<a class='dropdown-item clear_uploads' href='#' style='color:blue' tt='clear_upload_list'>clear</a>" ;
		}
		$.each ( me.upload_queue , function ( k , v ) {
			h += "<div class='dropdown-item'>" ;
			if ( v.is_uploaded ) {
				h += "<a href='" + escattr(v.data.file_url) + "' >" + me.escapeHTML(v.data.file) + "</a> &#10003;" ;
//				h += "<span style='color:green' tt='upload_done'>done</span>" ;
			} else if ( v.is_uploading ) h += "<span style='color:blue' tt='uploading'>uploading</span>" ;
			else if ( v.failed ) h += "<span style='color:red' tt='upload_fail_retry'>failed,retry</span>" ;
			else h += "<span tt='queueing'>queue</span>" ;
			h += "</div>" ;
		} ) ;
		$('#upload_list').html ( h ) ;
		$('#upload_list a.clear_uploads').click ( function () {
			me.clearUploads() ;
		} ) ;
		
		h = cnt_uploaded + '/' + me.upload_queue.length ;
		$('#dropdownUploads').html(h) ;
//		me.tt.updateInterface($('#dropdownUploadsLi')) ;
		$('#dropdownUploadsLi').show() ;
	} ,
	
	uploadNext : function () {
		var me = this ;
		if ( me.upload_queue.length == 0 ) return ;

		me.showUploadStatus() ;
		
		// Check if already uploading
		var i ;
		var is_uploading = false ;
		$.each ( me.upload_queue , function ( k , v ) {
			if ( v.is_uploading ) is_uploading = true ;
			else if ( v.is_uploaded == false && typeof i == 'undefined' ) i = k ;
		} ) ;
		if ( is_uploading ) return ;
		if ( typeof i == 'undefined' ) return ; // All done
		
		// Prepare upload
		var o = me.upload_queue[i] ;
		o.is_uploading = true ;

		
		// Uploading new file
		var opts = {
			url: wsm_comm.api_v3,
			data: o.data,
			cache: false,
			contentType: false,
			processData: false,
			dataType:'json',
			type: 'POST',
			success: function(d){
				me.upload_delay = 100 ; // Reset delay
				o.is_uploading = false ;
				if ( d.status == 'OK' ) {
					o.is_uploaded = true ;
					o.data = d.data ;
					me.switchItemToImageLayer ( d.data.q , d.data.file.replace(/_/g,' ') ) ;
				} else {
					o.failed = true ;
				}
			} ,
			error : function () {
				me.upload_delay += 1000 ; // Wait 1 second more after each error
			}
		};
		if(o.data.fake) {
			// Make sure no text encoding stuff is done by xhr
			opts.xhr = function() { var xhr = jQuery.ajaxSettings.xhr(); xhr.send = xhr.sendAsBinary; return xhr; }
			opts.contentType = "multipart/form-data; boundary="+o.data.boundary;
			opts.data = o.data.toString();
		}
		
		$.ajax(opts)
		.then ( function () {
			setTimeout ( function () { me.uploadNext() } , me.upload_delay ) ;
		} ) ;

		me.showUploadStatus() ;
	} ,
	
	pingLayer : function ( key ) {
		var me = this ;
		if ( -1 == $.inArray ( key , me.show_layers ) ) return ;
		me.map.removeLayer ( me.layers[key] ) ;
		me.map.addLayer ( me.layers[key] ) ;
	} ,
	
	switchItemToImageLayer : function ( q , image , form ) {
		var me = this ;
		var i = me.entries.wikidata[q] ;
		if ( typeof i == 'undefined'  ) { // Paranoia
			if ( typeof form != 'undefined' ) {
				form.replaceWith("<span>"+me.tt.t('image_added')+"</span>") ;
				$(form.parents('div.leaflet-popup-content').get(0)).find('div.add_image2item').remove() ;
			}
			return ;
		}
		i.image = image ;
		var marker = i.marker ;
		var was_open = true ; // Default
		if ( typeof marker.getPopup().isOpen != 'undefined' ) was_open = marker.getPopup().isOpen() ;
		me.layers.wikidata_image.addLayer(marker);
		me.layers.wikidata_no_image.removeLayer(marker);
		marker.setStyle ( {color:me.colors.wikidata_image,fillColor:me.colors.wikidata_image} ) ;
		me.pingLayer ( 'wikidata_image' ) ;
		marker.closePopup() ;
		marker.unbindPopup() ;
		marker.bindPopup ( me.createPopup ( i ) ) ;
		if ( was_open ) marker.openPopup() ;
	} ,
	
	addImageToItemHandler : function ( form ) {
		var me = this ;
		var q = form.find('input[name="q"]').val() ;
		var image = form.find('input[name="filename"]').val() ;
		wsm_comm.getWSM ( {
			action:'addImageToWikidata',
			image:image,
			q:q
		} , function ( d ) {
			if ( d.status != 'OK' ) {
				alert ( "ERROR: " + d.status ) ;
			} else {
				me.switchItemToImageLayer ( q , image , form ) ;
			}
		} ) ;
		return false ;
	} ,
	
	// Required properties of v:
	// lat, lon, title, ns, pageid
	addWikimediaEntry : function ( mode , server , v ) {
		var me = this ;
		var i = {
			pos : [ v.lat , v.lon ] ,
			label : mode=='commons' ? v.title.replace(/^File:/,'').replace(/\.[^.]+$/,'').replace(/_/g,' ') : v.title ,
			page : v.title ,
			mode : mode ,
			url : 'https://' + server + '/wiki/' + encodeURIComponent(v.title) ,
			server : server ,
			ns : v.ns
		} ;
		
		if ( mode == 'commons' ) i.image = v.title.replace(/^File:/,'') ;

		var bgcol = me.colors[mode] ;
		var scol = mode=='wikipedia' ? '#000' : bgcol ;
		var marker = L.circleMarker ( i.pos , {  stroke:true,color:scol,weight:1,fill:true,fillColor:bgcol,fillOpacity:me.opacity } ) ;
		marker.setRadius ( me.marker_radius[mode] ) ;
		marker.bindPopup ( me.createPopup ( i ) ) ;
		me.layers[mode].addLayer ( marker ) ;
		i.marker = marker ;
		me.entries[mode][''+v.pageid] = i ;
		return i ;
	} ,
	
	loadWikimediaLayer : function ( server , mode ) {
                return;
		var me = this ;
		var api = 'https://' + server + '/w/api.php' ;
		var b = me.map.getBounds() ;
		var nw = b.getNorthWest() ;
		var se = b.getSouthEast() ;

		me.loadCachedJSON ( api+'?callback=?' , {
			action:'query',
			list:'geosearch',
			gsbbox:nw.lat+'|'+nw.lng+'|'+se.lat+'|'+se.lng,
			gsnamespace:mode=='commons'?6:0,
			gslimit:500,
			format:'json'
		} , function ( d ) {

			$.each ( ((d.query||{}).geosearch||{}) , function ( dummy , v ) {
				me.addWikimediaEntry ( mode , server , v ) ;
			} ) ;

			if ( mode == 'commons' ) try { me.layers.commons.bringToBack() ; } catch ( e ) { }

		} ) ;

	} ,
	
	loadCachedJSON : function ( url , params , callback_success , callback_always ) {
		var me = this ;
		me.setBusy(1) ;
		console.log("loadCachedJSON");
		if ( typeof callback_always == 'undefined' ) callback_always = function() { me.setBusy(-1) } ;
		
		var do_cache = true ;
		var use_callback = false ; //url.match(/flickr/) ? true : false ;
		
		var key = JSON.stringify(params) ;
		
		if ( typeof me.json_cache[url] != 'undefined' ) {
			if ( me.json_cache[url].key == key ) {
				callback_success ( me.json_cache[url].result ) ;
				callback_always() ;
				return ;
			}
			me.json_cache[url] = {} ;
		}

		if ( use_callback ) {
			$.get ( url , params , function ( d ) {
				if ( do_cache ) me.json_cache[url] = { key : key , result : d } ;
				callback_success ( d ) ;
			} ) . always ( callback_always() ) ;
		} else {
			$.get ( url , params , function ( d ) {
				if ( do_cache ) me.json_cache[url] = { key : key , result : d } ;
				callback_success ( d ) ;
			} , 'json' ) . always ( callback_always() ) ;
		}
	} ,
	

	uploadURL2Commons : function ( url , title , desc , comment , pic ) {
		var me = this ;
		var params = {
			action:'upload',
			newfile:title,
			url:url,
			desc:desc,
			comment:comment,
			botmode:1
		} ;
	
	
		$.post ( '/magnustools/oauth_uploader.php?rand='+Math.random() , params , function ( d ) {
			$('#pleaseWaitDialog').modal('hide') ;
			
			if ( d.error == 'OK' ) {

				// Remove marker from original layer
				pic.marker.closePopup() ;
				pic.marker.unbindPopup() ;
				me.layers[pic.layer_key].removeLayer(pic.marker) ;

				// Add marker to Commons layer
				var new_file_name = d.res.upload.filename ;
				var new_pic = me.addWikimediaEntry ( 'commons' , 'commons.wikimedia.org' , {
					lat:pic.pos[0],
					lon:pic.pos[1],
					title:new_file_name,
					ns:6 ,
					pageid:'dummy_'+new_file_name
				} ) ;
				new_pic.marker.openPopup() ;

			} else {
				var s = [ d.error ] ;
				$.each ( (((d.res||{}).upload||{}).warnings||{}) , function ( k3 , v3 ) {
					if ( typeof v2 == 'array' ) {
						s.push ( k3 + ': ' + v3.join('; ') ) ;
					} else {
						s.push ( k3 + ': ' + v3 ) ;
					}
				} ) ;
				if ( typeof (((d.res||{}).error||{}).info) != 'undefined' ) s.push ( d.res.error.info ) ;
				
				alert ( "Transfer failed: " + s.join('; ') ) ;
				console.log ( s ) ;
			}

		} , 'json' )

		.fail(function(x) {
			alert ( "Transfer failed" ) ;
			console.log ( x ) ;
		});		
	
	} ,

	transferFlickr2Commons : function ( popup , flickr_id ) {
		var me = this ;
		var pic_num ;
		$.each ( me.flickr_pics , function ( k , v ) {
			if ( v.flickr_id != flickr_id ) return ;
			pic_num = k ;
			return false ;
		} ) ;
		if ( typeof pic_num == 'undefined' ) {
			console.log ( 'Flickr pic ' + flickr_id + ' not in cache' ) ;
			return false ;
		}
		var pic = me.flickr_pics[pic_num] ;
	
		var params = {
			id : flickr_id ,
			raw : 'on' ,
			format : 'json'
		} ;
	
		params.categories = ' ' ; // No auto categories

		$('#pleaseWaitDialog').modal() ;

		wsm_comm.getFlinfo ( params , function ( d ) {
			if ( undefined === d.wiki || d.wiki.status != 0 ) {
				var err = "Flinfo: " + d.wiki.status ;
				console.log ( err ) ;
				$('#pleaseWaitDialog').modal('hide') ;
				return ;
			}

			var final_desc = ( d.wiki.info.desc || '' ) ;
		
			var w = "== {{int:filedesc}} ==\n" ;
			w += "{{Information\n" ;
			w += "| Description = " + final_desc + "\n" ;
			w += "| Source      = " + ( d.wiki.info.source || '' ) + "\n" ;
			w += "| Date        = " + ( d.wiki.info.date || '' ) + "\n" ;
			w += "| Author      = " + ( d.wiki.info.author || '' ) + "\n" ;
			w += "| Permission  = " + ( d.wiki.info.permission || '' ) + "\n" ;
			w += "| other_versions=\n" ;
			w += "}}\n" ;
		
			if ( undefined !== d.wiki.geolocation && undefined !== d.wiki.geolocation.latitude ) {
				w += "{{Location dec|" + d.wiki.geolocation.latitude + "|" + d.wiki.geolocation.longitude + "|source:" + d.wiki.geolocation.source + "}}\n" ;
			} else {
				w += "{{Location dec|" + pic.pos[0] + "|" + pic.pos[1] + "|source:Flickr}}\n" ;
			}
		
			w += "\n=={{int:license-header}}==\n" ;
			$.each ( ( d.wiki.licenses || [] ) , function ( k , v ) {
				w += "{{" + v + "}}\n" ;
			} ) ;
		
			w += "\n" ;
			$.each ( ( d.wiki.categories || [] ) , function ( k , v ) {
				w += "[[" + v + "]]\n" ;
			} ) ;
		
			w = $.trim ( w ) ;
			
			var title = $.trim(pic.label) ;
			if ( title == '' || title.match(/^(IMG|DSC){0,1}[0-9 _]*$/) ) title = 'Flickr image' ;
			title += ' ' + flickr_id ;
			title += '.jpg' ;
			
			var comment = 'Transferred from Flickr via [https://tools.wmflabs.org/wikishootme WikiShootMe] #wikishootme' ;
		
			me.uploadURL2Commons ( pic.url_best , title , w , comment , pic ) ;
		
		} , 'json' ) ;

		return false ;
	} ,

	loadFlickrLayer2 : function () {
		var me = this ;

		var b = me.map.getBounds() ;
		var sw = b.getSouthWest() ;
		var ne = b.getNorthEast() ;

		var params = {
			method:'flickr.photos.search',
			api_key:me.flickr_api_key,
			license:'4,5,7,8,9,10',
			sort:'interestingness-desc',
			bbox:b.toBBoxString(),
			nojsoncallback:1,
			per_page:250,
			extras:'description,geo,url_s,url_o,url_l,url_m',
			format:'json'
		} ;

		var me = this ;
		me.loadCachedJSON ( 'https://api.flickr.com/services/rest/' , params , function ( d ) {
			me.flickr_pics = [] ;
			var bgcol = me.colors['flickr'] ;
			$.each ( d.photos.photo , function ( k , v ) {
				if ( v.ispublic != 1 ) return ;
				var i = {
					flickr_id : v.id ,
					mode:'flickr',
					label:v.title,
					description:v.description['_content'],
					thumburl:v.url_s,
					layer_key:'flickr',
					pos:[v.latitude,v.longitude],
					url:'https://www.flickr.com/photos/'+v.owner+'/'+v.id
				} ;
				if ( undefined !== v.url_o ) i.url_best = v.url_o ;
				else if ( undefined !== v.url_l ) i.url_best = v.url_l ;
				else if ( undefined !== v.url_m ) i.url_best = v.url_m ;
				else return ; // No good resolution available
				var pos = [ v.latitude*1 , v.longitude*1 ] ;
				var marker = L.circleMarker ( pos , { stroke:true,color:bgcol,weight:1,fill:true,fillColor:bgcol,fillOpacity:me.opacity } ) ;
				marker.setRadius ( me.marker_radius['flickr'] ) ;
				marker.bindPopup ( me.createPopup ( i ) ) ;
				me.layers[i.layer_key].addLayer ( marker ) ;
				i.marker = marker ;
				me.flickr_pics.push ( i ) ;
			} ) ;
		} ) ;
	} ,
	
	loadFlickrLayer : function () {
		var me = this ;

		// Load Flickr key, if necessary
		if ( typeof me.flickr_api_key == 'undefined' ) {
			wsm_comm.getFlickrKey ( function ( d ) {
				me.flickr_api_key = $.trim(d) ;
				me.loadFlickrLayer2() ;
			} ) ;
		} else {
			me.loadFlickrLayer2() ;
		}

	} ,
	
	loadGeoJsonLayer : function () {
		var me = this ;

		var params = {};
		var skip=1;
		if (typeof me.geojson_file !== "undefined" ) {
                        for (var i = 0; i < me.allowed_geojson_domains.length; i++) {
                            re = new RegExp("\/\/[^?/]*?" +  me.allowed_geojson_domains[i] + "([?/]|$)");
			    var m=re.test(me.geojson_file);
			    if (m) skip=0;
			}
                   }
		console.log ( skip ) ;
                if (skip==0 && (typeof me.geojson_loaded === "undefined" || me.zoom_level<10))
			me.loadCachedJSON ( me.geojson_file , params , function ( d ) {
				me.layers['geojson'].addData(d);
				me.geojson_loaded=true; 
				me.centerToLayer(me.layers['geojson']);
/*				if (typeof me.geojson_center !== "undefined" 
				   && typeof me.geojson_center_done === "undefined"  )
				{
					me.geojson_center_done=1;
					var bounds = me.layers['geojson'].getBounds();
					var p = bounds.getCenter();
					me.pos.lat = p.lat;
					me.pos.lng = p.lng;
					me.zoom_level=me.map.getBoundsZoom(bounds);
					me.setMap();
				}*/
			} ) ;
	} ,
	loadMixNMatchLayer : function () {
		var me = this ;

		var b = me.map.getBounds() ;
		var sw = b.getSouthWest() ;
		var ne = b.getNorthEast() ;

		var params = {
			query : 'locations' ,
			bbox:b.toBBoxString()
		} ;
//		console.log ( params ) ;

		me.loadCachedJSON ( 'https://tools.wmflabs.org/mix-n-match/api.php' , params , function ( d ) {
//			console.log ( d ) ;
			me.mixnmatch_entries = [] ;
			var bgcol = me.colors['mixnmatch'] ;
			$.each ( d.data , function ( k , v ) {
				var i = {
					entry : v.entry ,
					catalog : v.catalog ,
					label:v.ext_name,
					layer_key:'mixnmatch',
					description:v.ext_desc,
					pos:[v.lat,v.lon],
					mixnmatch:{q:v.q,user:v.user},
					url:'https://tools.wmflabs.org/mix-n-match/#/entry/'+v.entry
				} ;
				var pos = [ v.lat*1 , v.lon*1 ] ;
				var marker = L.circleMarker ( pos , { stroke:true,color:bgcol,weight:1,fill:true,fillColor:bgcol,fillOpacity:me.opacity } ) ;
				marker.setRadius ( me.marker_radius['mixnmatch'] ) ;
				marker.bindPopup ( me.createPopup ( i ) ) ;
				me.layers[i.layer_key].addLayer ( marker ) ;
				i.marker = marker ;
				me.mixnmatch_entries.push ( i ) ;
			} ) ;
		} ) ;
	} ,
	loadSingleFromSparql : function(row, wrapped) {
                var me = this;
                var b = me.map.getBounds() ;
                var ne = b.getNorthEast() ;
                var sw = b.getSouthWest() ;

		if (typeof row == 'undefined') return;
                if (row.item) {
                    sparql_rule = "BIND(<" + row.item + "> AS ?q)";
                } else if (row["?item"]) {
                    sparql_rule = "BIND(" + row["?item"] + " AS ?q)";
                } else if (typeof row['url'] != 'undefined') {
                    var url = new URL(row['url']);
                    if (!(url.hostname.includes('rky.fi') || url.hostname === 'rky.fi')) return;
	    	    if (typeof row['ID'] == 'undefined' ) return
		    if (!Number.isInteger(row['ID'])) return
                    sparql_rule = " ?q wdt:P4009 '" + row['ID'] +"'"
		}
		else if (typeof row['s_matkailu'] != 'undefined') {
                    console.log(typeof row['s_matkailu'] != 'undefined');
                    sparql_rule = " ?q wdt:P528 '" + row['s_matkailu'] +"' ."
                }
		else if (typeof row['museonimi'] != 'undefined') {
                    console.log(typeof row['museonimi'] != 'undefined');
                    sparql_rule = " ?q wdt:P528 '" + row['museonimi'] +"' ."
                }
		else {
		    return
		}
                
		
//#  BIND( wd:Q11864050 as ?q ) 

		var sparql = "\
SELECT\
?q\
?qLabel ?qLabel_fi ?qLabel_en ?qLabel_sv ?qLabel_sms ?qLabel_smn ?qLabel_se ?qLabel_es\
?desc_fi ?desc_sv ?desc_en ?desc_se ?desc_sms ?desc_smn ?desc_es\
?sitelink_fi ?sitelink_sv ?sitelink_en ?sitelink_se ?sitelink_sms ?sitelink_smn ?sitelink_es\
\
?location ?image ?reason ?desc ?commonscat ?geoshape ?p4009 ?p5310 ?p5313 ?p4106 ?p8355 WHERE {\
\
  "+ sparql_rule +"\
\
  OPTIONAL { ?q wdt:P625 ?location }\
  OPTIONAL { ?q wdt:P3896 ?geoshape }\
  OPTIONAL { ?q wdt:P18 ?image }\
  OPTIONAL { ?q wdt:P373 ?commonscat }\
  OPTIONAL { ?q wdt:P4009 ?p4009 }\
  OPTIONAL { ?q wdt:P5310 ?p5310 }\
  OPTIONAL { ?q wdt:P5313 ?p5313 }\
  OPTIONAL { ?q wdt:P4106 ?p4106 }\
  OPTIONAL { ?q wdt:P8355 ?p8355 }\
  OPTIONAL { ?q rdfs:label ?qLabel_fi . FILTER(LANG(?qLabel_fi) = 'fi') }\
  OPTIONAL { ?q schema:description ?desc_fi . FILTER(LANG(?desc_fi) = 'fi') }\
\
  OPTIONAL { ?q rdfs:label ?qLabel_sv . FILTER(LANG(?qLabel_sv) = 'sv') }\
  OPTIONAL { ?q schema:description ?desc_sv . FILTER(LANG(?desc_sv) = 'sv') }\
\
  OPTIONAL { ?q rdfs:label ?qLabel_en . FILTER(LANG(?qLabel_en) = 'en') }\
  OPTIONAL { ?q schema:description ?desc_en . FILTER(LANG(?desc_en) = 'en') }\
\
  OPTIONAL { ?q rdfs:label ?qLabel_se . FILTER(LANG(?qLabel_se) = 'se') }\
  OPTIONAL { ?q schema:description ?desc_se . FILTER(LANG(?desc) = 'se') }\
\
  OPTIONAL { ?q rdfs:label ?qLabel_sms . FILTER(LANG(?qLabel_sms) = 'sms') }\
  OPTIONAL { ?q schema:description ?desc_sms . FILTER(LANG(?desc_sms) = 'sms') }\
\
  OPTIONAL { ?q rdfs:label ?qLabel_smn . FILTER(LANG(?qLabel_smn) = 'smn') }\
  OPTIONAL { ?q schema:description ?desc_smn . FILTER(LANG(?desc_smn) = 'smn') }\
\
  OPTIONAL { ?q rdfs:label ?qLabel_es . FILTER(LANG(?qLabel_es) = 'es') }\
  OPTIONAL { ?q schema:description ?desc_es . FILTER(LANG(?desc_es) = 'es') }\
\
  OPTIONAL { ?sitelink_fi schema:isPartOf <https://fi.wikipedia.org/>; schema:about ?q; }\
  OPTIONAL { ?sitelink_sv schema:isPartOf <https://sv.wikipedia.org/>; schema:about ?q; }\
  OPTIONAL { ?sitelink_en schema:isPartOf <https://en.wikipedia.org/>; schema:about ?q; }\
  OPTIONAL { ?sitelink_se schema:isPartOf <https://se.wikipedia.org/>; schema:about ?q; }\
  OPTIONAL { ?sitelink_sms schema:isPartOf <https://incubator.wikimedia.org/wiki/Wp/sms>; schema:about ?q; }\
  OPTIONAL { ?sitelink_smn schema:isPartOf <https://sms.wikipedia.org/>; schema:about ?q; }\
  OPTIONAL { ?sitelink_es schema:isPartOf <https://es.wikipedia.org/>; schema:about ?q; }\
\
  OPTIONAL {\
     ?q wdt:P361 ?mainitem .\
     ?mainitem wdt:P4009 ?mainitem_p4009\
  }\
  OPTIONAL {\
     ?q wdt:P361 ?mainitem .\
     ?mainitem wdt:P5310 ?mainitem_p5310\
  }\
  OPTIONAL {\
     ?q wdt:P361 ?mainitem .\
     ?mainitem wdt:P4106 ?mainitem_p4106\
  }\
}\
LIMIT 10\
";
		console.log(sparql);
		console.log(me.sparql_url + "?format=json&query=" + encodeURIComponent(sparql));
		me.loadCachedJSON (  me.sparql_url , {query:sparql} , function ( d ) {
                        if ( typeof d == 'undefined' || typeof d.results == 'undefined' || typeof d.results.bindings == 'undefined' ) return ;
                        $.each ( d.results.bindings , function ( dummy , item ) {
				console.log(item);
				if ( item.q.type != 'uri' ) return ;
				var q = item.q.value.replace ( /^.+\// , '' ) ;
				if ( typeof me.entries.wikidata[q] != 'undefined' ) {
					i =  me.entries.wikidata[q]
				} else {
	                                i = me.parseWikidataItem(item, wrapped);
				}
                                console.log(q)
				if ( typeof i == 'undefined') return;
				var popup = me.createPopup(i)
                                popup.setLatLng(wrapped)
				popup.openOn(me.map);
			});
		});
        },
        parseWikidataItem : function (item, wrapped) {
                                me = this;
				if ( item.q.type != 'uri' ) return ;
				var q = item.q.value.replace ( /^.+\// , '' ) ;
				if ( typeof me.entries.wikidata[q] != 'undefined' ) return ; // Multiple locations/images; use first one only
				var col
				var i = {
					page:q ,
					label:q ,
					qid: q,
					mode:'wikidata' ,
					url:'https://www.wikidata.org/wiki/' + q ,
					ns:0,
                                        col:col
				} ;
				if ( typeof item.image != 'undefined' ) {
					if ( item.image.type == 'uri' ) {
						i.image = decodeURIComponent ( item.image.value.replace(/^.+\//,'') ) ;
					}
				} else if ( me.check_reason_no_image && typeof item.reason != 'undefined' && item.reason.type == 'uri' ) {
					var reason = item.reason.value.replace(/^.+\//,'') ;
					if ( reason == 'Q15687022' ) {
						i.col = me.colors.no_image_copyright ;
						opacity = 1 ;
						i.note = me.tt.t('no_image_copyright') ;
						i.no_image = true ;
					} else {
						return ; // No value for other reasons, don't show (probably futile)
					}
				}
                                var langcodes = [me.language, "en", "fi", "sv", "se", "smn", "sms"];
                              	for (var l = 0; l < langcodes.length; l++) {
				    label_langcode_key="qLabel_" + langcodes[l];
  				    if ( typeof item[label_langcode_key] != 'undefined' && item[label_langcode_key].type == 'literal' ) i.label = item[label_langcode_key].value ;
                                    if (i.label!=q) break;
				}

                              	for (var l = 0; l < langcodes.length; l++) {
				    desc_langcode_key="desc_" + langcodes[l];
 				    if ( typeof item[desc_langcode_key] != 'undefined' && item[desc_langcode_key].type == 'literal' ) i.description = item[desc_langcode_key].value ;
                                    if (typeof i.description != 'undefined' && i.description!="") break;
				}
                              	for (var l = 0; l < langcodes.length; l++) {
				    sitelink_langcode_key="sitelink_" + langcodes[l];
 				    if ( typeof item[sitelink_langcode_key] != 'undefined' && item[sitelink_langcode_key].type == 'uri' ) i.sitelink = item[sitelink_langcode_key].value ;
                                    if (typeof i.sitelink != 'undefined' && i.sitelink!="") break;
				}
				if ( typeof item.commonscat != 'undefined' && item.commonscat.type == 'literal' ) i.commonscat = item.commonscat.value ;
//				if ( typeof item.geoshape != 'undefined'  ) { i.geoshape = item.geoshape.value ;}
				if ( typeof item.p4009 != 'undefined'  ) { i.p4009 = item.p4009 ;}
				if ( typeof item.p5313 != 'undefined'  ) { i.p5313 = item.p5313 ;}
				if ( typeof item.p5310 != 'undefined'  ) { i.p5310 = item.p5310 ;}
				if ( typeof item.p4106 != 'undefined'  ) { i.p4106 = item.p4106 ;}
				if ( typeof item.p8355 != 'undefined'  ) { i.p8355 = item.p8355 ;}
				if ( typeof item.location != 'undefined' && item.location.type == 'literal' && item.location.datatype == "http://www.opengis.net/ont/geosparql#wktLiteral" ) {
					var m = item.location.value.match ( /^Point\((.+?)\s(.+?)\)$/ ) ;
					if ( m != null ) i.pos = [ m[2]*1 , m[1]*1 ] ;
				}
				if ( typeof i.pos == 'undefined' ) {
					if (typeof wrapped != 'undefined' && wrapped) i.pos = wrapped
					else return ;
				}
                                return i

        },
	loadWikidataLayer : function () {
		var me = this ;
		var b = me.map.getBounds() ;
		var ne = b.getNorthEast() ;
		var sw = b.getSouthWest() ;
		var sparql = "#TOOL: WikiShootMe\n" ;
		sparql += 'SELECT ?q ?qLabel ?location ?image ?reason ?desc ?commonscat ?geoshape ?p4009 ?p5310 ?p5313 ?p4106 ?p8355 WHERE { ' ;
		if ( me.worldwide ) {
			sparql += "?q wdt:P625 ?location . " ;
		} else {
			sparql += 'SERVICE wikibase:box { ?q wdt:P625 ?location . ' ;
			sparql += 'bd:serviceParam wikibase:cornerSouthWest "Point('+sw.lng+' '+sw.lat+')"^^geo:wktLiteral . ' ;
			sparql += 'bd:serviceParam wikibase:cornerNorthEast "Point('+ne.lng+' '+ne.lat+')"^^geo:wktLiteral } ' ;
		}
		sparql += me.sparql_filter ;
		sparql += ' OPTIONAL { ?q wdt:P3896 ?geoshape }';
		sparql += ' OPTIONAL { ?q wdt:P18 ?image } ' ;
		sparql += ' OPTIONAL { ?q wdt:P373 ?commonscat } ' ;
                sparql += ' OPTIONAL { ?q wdt:P4009 ?p4009 } ';
                sparql += ' OPTIONAL { ?q wdt:P5310 ?p5310 } ';
                sparql += ' OPTIONAL { ?q wdt:P5313 ?p5313 } ';
                sparql += ' OPTIONAL { ?q wdt:P4106 ?p4106 } ';
                sparql += ' OPTIONAL { ?q wdt:P8355 ?p8355 } ';
		if ( me.check_reason_no_image ) sparql += 'OPTIONAL { ?q p:P18 ?statement . ?statement pq:P828 ?reason } ' ;
		
		if ( me.worldwide ) {
			sparql += ' OPTIONAL { ?q rdfs:label ?qLabel . FILTER(LANG(?qLabel) = "'+me.language+'") } OPTIONAL { ?q schema:description ?desc . FILTER(LANG(?desc) = "'+me.language+'") } ' ;
		} else {
			sparql += ' SERVICE wikibase:label { bd:serviceParam wikibase:language "'+me.language+',fi,sv,en,no,de,fr,es,it,nl,ru" . ?q schema:description ?desc . ?q rdfs:label ?qLabel } ' ;
		}
		sparql += "} LIMIT " + (me.worldwide?25000:3000) ;
		console.log(sparql);
		console.log(me.sparql_url + "?format=json&query=" + encodeURIComponent(sparql));
		var opacity = me.opacity ;

//		me.loadCachedJSON ( me.sparql_url , {
//			query:sparql
		/*me.loadCachedJSON ( 'wlm_items_multilang.json?foo' , function ( d ) {
			if ( typeof d == 'undefined' || typeof d.results == 'undefined' || typeof d.results.bindings == 'undefined' ) return ;
			$.each ( d.results.bindings , function ( dummy , item ) {

				i = me.parseWikidataItem(item);
                                if ( typeof i == 'undefined') return;

				var has_image = ( typeof i.image != 'undefined' ) ;
				if ( typeof i.col == 'undefined' ) col = has_image ? me.colors.wikidata_image : me.colors.wikidata_no_image ;
				var marker = L.circleMarker ( i.pos , {  stroke:true,color:col,weight:1,fill:true,fillColor:col,fillOpacity:opacity } ) ;
				marker.setRadius ( me.marker_radius.wikidata ) ;
                                if (L.Browser.mobile ) {
					marker.bindPopup ( me.createPopup ( i ) ) ;
				}
				else
				{
					marker.bindPopup ( me.createPopup ( i ) ) ;
	                                marker.bindTooltip(i.label);
				}
				if ( has_image ) me.layers.wikidata_image.addLayer(marker);
				else me.layers.wikidata_no_image.addLayer(marker);
				i.marker = marker ;
				me.entries.wikidata[i.qid] = i ;
				if ((typeof i.geoshape!= 'undefined') && (!me.geoshapes.includes(i.geoshape))) {
					me.geoshapes.push(i.geoshape);
					me.loadCachedJSON("get_data.php?filename=" + i.geoshape.replace('+', '_').replace('http://commons.wikimedia.org/data/main/', ''), {}, function(d) {
						if (typeof d == 'undefined' || typeof d.data == 'undefined') return;
						var geo=L.geoJSON(d.data).addTo(me.layers.geoshape);
					});
				}
			} ) ;
		//	var center_l=new L.FeatureGroup(me.layers.wikidata_image, me.layers.wikidata_no_image);	
			if (!me.centerToLayer(me.layers.wikidata_image)) me.centerToLayer(me.layers.wikidata_no_image);
		} ) ;*/
	} ,
	centerToLayer:function(layer) {
		var me=this;
		if (typeof me.geojson_center !== "undefined" 
			&& typeof me.geojson_center_done === "undefined"  )
		{
			me.geojson_center_done=1;
			var bounds = layer.getBounds();
			if (Object.keys(bounds).length>0){
				var p = bounds.getCenter();
				if (typeof(p)!=="undefined" && typeof(p.lat)!=="undefined")
				{
					me.pos.lat = p.lat;
					me.pos.lng = p.lng;
					me.zoom_level=me.map.getBoundsZoom(bounds);
					me.setMap();


					return true;
				}
			}
			return false;
		}
		return true;
	},

	updateMarkerMe : function ( p ) {
		var me = this ;
		if ( !navigator.geolocation ) return ;
		if ( typeof me.marker_me == 'undefined' ) {
			me.marker_me = L.circleMarker ( [p.lat,p.lng] , {  stroke:true,color:me.colors.me,weight:1,fill:true,fillColor:me.colors.me,fillOpacity:me.opacity } ) ;
			me.marker_me.setRadius ( me.marker_radius.me ) ;
			me.marker_me.bindPopup ( L.popup().setContent(me.tt.t('we_know')) ) ;
			me.marker_me.addTo ( me.map ) ;
			return ;
		}
		me.marker_me.setLatLng ( [ p.lat , p.lng ] ) ;
	} ,
	
	loadLayer : function ( key ) {
		var me = this ;
		console.log("loadLayer:" + key);
		if ( 0 ) {
			me.setBusy(1) ;
			setTimeout ( function () { me.setBusy(-1) } , 10 ) ;
			return ;
		}
		


		var is_visible = (-1 != $.inArray ( key , me.show_layers )) ;
		if ( key == 'wikidata' && !is_visible ) is_visible = (-1!=$.inArray('wikidata_image',me.show_layers )) || (-1!=$.inArray('wikidata_no_image',me.show_layers )) ;
		if (  !is_visible ) {
			me.setBusy(1) ;
			setTimeout ( function () { me.setBusy(-1) } , 10 ) ;
			return ;
		}
		
		if ( key.match(/^wikidata_/) ) key = 'wikidata' ;
		
		if ( key == 'wikidata' ) me.loadWikidataLayer() ;
		if ( key == 'commons' ) me.loadWikimediaLayer ( 'commons.wikimedia.org' , 'commons' ) ;
		if ( key == 'wikipedia' ) me.loadWikimediaLayer ( me.language+'.wikipedia.org' , 'wikipedia' ) ;
		if ( key == 'flickr' ) me.loadFlickrLayer () ;
		if ( key == 'mixnmatch' ) me.loadMixNMatchLayer () ;
		//if ( key == 'geojson' ) me.loadGeoJsonLayer () ;
//		if ( key == 'pmtiles' ) me.loadPmtilesLayer () ;
		me.updatePermalink() ;
		me.layers.geoshape.bringToBack();
//		me.layers.pmtiles.bringToFront();


	} ,
	
	updateLayers : function () {
		var me = this ;
		$('#update').hide() ;
		var z=me.map.getZoom();
                if (me.worldwide && typeof(me.entries.wikidata)!="undefined" && Object.keys(me.entries.wikidata).length) return;
		if (z<10)
			me.cleanLayers() ;
		
		$.each ( ['commons','wikipedia','wikidata','flickr','mixnmatch', 'geojson'] , function ( k , v ) {
//		$.each ( ['wikidata'] , function ( k , v ) {
			me.loadLayer ( v ) ;
		} ) ;
			me.layers.geoshape.bringToBack();
//			me.layers.pmtiles.bringToFront();

	} ,
	
	updateToCurrent : function () {
		var me = this ;
		var b = me.map.getBounds() ;
		me.pos = b.getCenter() ;
		me.updateLayers() ;
	} ,
	
	updateMaybe : function () {
		var me = this ;
		var z = me.map.getZoom() ;
		//if ( z > 12 ) me.updateToCurrent() ;
		//else $('#update').show() ;
	} ,
	
	gps2leaflet : function ( gps ) {
		return { lat:gps.latitude , lng:gps.longitude } ;
	} ,
	

	addMarkerMe : function () {
		var me = this ;
	        var geolocationErrorHandler = function (error) {
			console.error(error.message);
		};

		if ( navigator.geolocation ) {
			var options = {
				enableHighAccuracy: true,
				timeout: 5000,
				maximumAge: 0
			};
			me.updateMarkerMe ( me.pos ) ;
			navigator.geolocation.watchPosition(function(position) { me.updateMarkerMe ( me.gps2leaflet(position.coords) ) }, geolocationErrorHandler, options ) ;
		}
	} ,
	
	addNewWikidataItem : function ( q , label , pos , image ) {
		var me = this ;
		var i = {
			page:q ,
			label:label ,
			mode:'wikidata' ,
			url:'https://www.wikidata.org/wiki/' + q ,
			pos:[pos.lat,pos.lng],
			ns:0
		} ;
		
		var layer = 'wikidata_no_image' ;
		if ( typeof image != 'undefined' && image != '' ) {
			layer = 'wikidata_image' ;
			i.image = image ;
		}

		var col = me.colors[layer] ;
		var marker = L.circleMarker ( i.pos , {  stroke:true,color:col,weight:1,fill:true,fillColor:col,fillOpacity:me.opacity } ) ;
		marker.setRadius ( me.marker_radius.wikidata ) ;
		marker.bindPopup ( me.createPopup ( i ) ) ;
		me.layers[layer].addLayer(marker);

		i.marker = marker ;
		me.entries.wikidata[q] = i ;
		// me.pingLayer ( layer ) ;
		return marker ;
	} ,
	
	createMap : function () {
		var me = this ;
		if ( me.map_is_set ) return false ; // No map created
		
		// Create the map
		me.map_is_set = true ;
		me.map = L.map('map', {
                        contextmenu: false,
                        preferCanvas: true,
			contextmenuWidth: 250,
			contextmenuItems: [/*{
				text:me.tt.t('create_new_item_from_coordinate'),
				callback: function ( ev ) { me.createNewItem ( { pos:ev.latlng, label_default:''} ) }
			},*/{
				text:me.tt.t('show_coordinates'),
				callback: function ( ev ) { alert(ev.latlng); }			
			}/*,{
				text:me.tt.t('show_coordinates'),
				callback: function ( ev ) { alert("foo"); }
			}*/]
		}).setView ( [ me.pos.lat , me.pos.lng ] , me.zoom_level ) ;
		L.tileLayer('https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png?lang=' + me.language, {
			attribution: "&copy; <a target='" + me.href_target +"' href='http://osm.org/copyright'>OpenStreetMap</a> contributors"
		}).addTo(me.map);
		me.map.on ( 'viewreset' , function () { me.updateMaybe() } ) ;
		me.map.on ( 'zoomend' , function () {
			var z = me.map.getZoom() ;
			if ( z > me.zoom_level ) {
				me.zoom_level = z ; // No need to reload data
				me.updatePermalink() ;
			} else {
				me.updateMaybe() ;
			}
		} ) ;
		me.map.on ( 'dragend' , function () { me.updateToCurrent() } ) ;



		// Pop-up open handler, loads pageimage for Wikipedia
		me.map.on ( 'popupopen' , function ( pe ) {
			var popup = pe.popup ;
			var c = popup.getContent() ;
			if ( c == me.tt.t('we_know') ) return ;
			c = $(c) ;
			
			c.find('div.transfer2flickr').each ( function () { // Flickr transfer function
				var div = $(this) ;
				var html_do_upload = "<a href='#' onclick='wikishootme.transferFlickr2Commons(this,\""+div.attr('flickr_id')+"\");return false'>Transfer from Flickr to Commons</a>" ;
				if ( wsm_comm.oauth_uploader_login ) {
					div.html ( html_do_upload ) ;
					h = c.html() ;
					popup.setContent ( h ) ;
				} else {
					$.get ( '/magnustools/oauth_uploader.php?action=checkauth&botmode=1' , function ( d ) {
						if ( d.error == 'OK' ) {
							wsm_comm.oauth_uploader_login = true ;
							div.html ( html_do_upload ) ;
						} else {
							div.html ( d.error ) ;
						}
						h = c.html() ;
						popup.setContent ( h ) ;
					} , 'json' ) ;
				}
			} ) ;

			
			c.find('div.pageimage_toload').each ( function () { // Lazy-load Commons image
				var div = $(this) ;
				var server = div.attr ( 'server' ) ;
				var page = div.attr ( 'page' ) ;
				var url = 'https://'+server+'/w/api.php?callback=?' ;
				$.getJSON ( url , {
					action:'mobileview',
					page:page,
					prop:'image',
					thumbsize:me.thumb_size,
					format:'json'
				} , function ( d ) {
					if ( typeof d.mobileview == 'undefined' || typeof d.mobileview.image == 'undefined' ) return ;
					var h = me.createImageThumbnail ( d.mobileview.image.file ) ;
					div.replaceWith ( h ) ;
					h = c.html() ;
					popup.setContent ( h ) ;
				} ) ;
			} ) ;
		} ) ;
		

		// Create layers
		$.each ( me.layer_info.key2name , function ( key , name ) {
			name = "<div style='display:inline-block;background-color:"+me.colors[key]+";border:1px solid "+me.colors[key]+";width:12px;height:12px;padding-top:3px;padding-right:3px;opacity:"+me.opacity+";'></div> " + name ;
			me.layer_info.key2name[key] = name ;
			me.layer_info.name2key[name] = key ;
			if (key=='geojson')
			{
				var onEachFeature = function(feature, layer) {
    				// does this feature have a property named popupContent?
    					if (feature.properties && feature.properties.popupContent) {
        					layer.bindPopup("<p>" + feature.properties.popupContent + "</p>");
    					}
				};

				var pointToLayer=function(feature, ll) {
                                        var style={}
    					if (feature.properties && feature.properties.color) {
        					style.color=feature.properties.color;  
    					}

        				var featureForThisPoint = L.circleMarker(ll, style);
        				return featureForThisPoint;
				};
				me.layers[key] = L.geoJSON(false, { onEachFeature: onEachFeature, pointToLayer: pointToLayer} );
			}
			else if (key=='geojson')
			{
				me.layers[key] = L.LayerGroup() ;
			}
                        else if (key=='rky_area')
                        {
                                let PAINT_RULES = [
							{
								dataLayer: "wikidata_osm_areas",
								symbolizer: new protomapsL.PolygonSymbolizer({
									fill: "rgba(0, 255, 0, 0.3)",
									width: 2,
									stroke: "rgba(0, 255, 0, 0.6)",
								})
							},
						];
				let LABEL_RULES = []; // ignore for now
				//pmurl = "https://fiwiki-tools.toolforge.org/rky_area.pmtiles"
				pmurl = "https://tools-static.wmflabs.org/commons-upload-map/tiledata/wikidata_osm_areas.pmtiles"
				me.layers[key] = protomapsL.leafletLayer({
					url: pmurl,
					maxDataZoom: 11,
					paintRules: PAINT_RULES,
					labelRules: LABEL_RULES
				});
                        }
                        else if (key=='rky_lines')
                        {
                                let PAINT_RULES = [
							{
								dataLayer: "wikidata_points",
								symbolizer: new protomapsL.CircleSymbolizer({
									radius: 10,
									fill: "rgba(0, 0, 255, 0.3)",
									width: 3,
									stroke: "rgba(0, 0, 255, 0.6)",
								})
							}
						];
				let LABEL_RULES = []; // ignore for now
                                pmurl = "https://tools-static.wmflabs.org/commons-upload-map/tiledata/wikidata_points.pmtiles"
				me.layers[key] = protomapsL.leafletLayer({
					url: pmurl,
					maxDataZoom: 5,
					paintRules: PAINT_RULES,
					labelRules: LABEL_RULES
				});
                        }
                        else if (key=='museumroads')
                        {
                                let PAINT_RULES = [
							{
								dataLayer:"museumroads_and_travelroadsgeojsonl",
								symbolizer:new protomapsL.LineSymbolizer({color:"steelblue", width:5})
							},
						];
				let LABEL_RULES = []; // ignore for now
                                pmurl = "https://fiwiki-tools.toolforge.org/museumroads_and_travelroads.pmtiles"
				me.layers[key] = protomapsL.leafletLayer({url:pmurl, paintRules:PAINT_RULES, labelRules:LABEL_RULES})
				
                        }
                        else if (key=="wikidata_no_image" || key=="wikidata_image") {
                                 me.layers[key]= L.markerClusterGroup({
                                     spiderfyOnMaxZoom: false,
                                     chunkedLoading: true,
                                     disableClusteringAtZoom: 8,
                                     maxClusterRadius: 80,
                                     showCoverageOnHover: false,
                                     zoomToBoundsOnClick: true
                                } );
                         }
			else
			{
				me.layers[key] = L.featureGroup() ;
			}

			//if (key=="wikidata_no_image" || key=="wikidata_image") 
			me.overlays[name] = me.layers[key] ;
			if ( -1 != $.inArray ( key , me.show_layers ) ) {
				me.layers[key].addTo ( me.map ) ;
			}
		} ) ;
//		me.layer_control = L.control.layers(null, me.overlays).addTo(me.map);
		
		me.map.on('overlayadd', function(e){me.onOverlayAdd(e)});
		me.map.on('overlayremove', function(e){me.onOverlayRemove(e)});

                me.map.on("click", (ev) => {
                    const wrapped = me.map.wrapLatLng(ev.latlng);
                    // note: this method supports only basic use,
                    // see comments in source code

                        newfeatures=[]
                        for (let [key, name] of Object.entries( me.layer_info.key2name)) {
                                layer=me.layers[key]
                                if (typeof(layer.queryTileFeaturesDebug)!="function") continue
				featuregroups = layer.queryTileFeaturesDebug(wrapped.lng, wrapped.lat);
 				featuregroups.forEach((features, keys) => {
					for (i=0; i<features.length;i++) {
 						feature = features[i]
 						newfeatures.push(feature['feature']['props']);
					}
 				});
                	}
                        console.log(newfeatures);
			me.loadSingleFromSparql(newfeatures[0], wrapped);
		})


		me.addMarkerMe() ;
		return true ;
	} ,
	
	createNewItem : function ( o ) {
		var me = this ;
		if ( typeof o.label_default == 'undefined' ) o.label_default = '' ;
		if ( typeof o.image == 'undefined' ) o.image = '' ;
		var rlat = Math.round ( o.pos.lat * 10000 ) / 10000 ;
		var rlng = Math.round ( o.pos.lng * 10000 ) / 10000 ;
		var label = prompt ( me.tt.t ( 'create_new_item' , {params:[rlat,rlng]} ) , o.label_default ) ;
		if ( label == null || label == '' ) return ;
		
		me.getAdminUnit ( o.pos.lat , o.pos.lng , function ( p131 ) {
			wsm_comm.getWSM ( {
				action:'new_item',
				lat:o.pos.lat,
				lng:o.pos.lng,
				p131:p131,
				p18:o.image,
				label:label,
				lang:me.language
			} , function ( d ) {
				if ( d.status != 'OK' ) {
					alert ( "ERROR: " + d.status ) ;
					return ;
				}
				var marker = me.addNewWikidataItem ( d.q , label , o.pos , o.image ) ;
				marker.openPopup() ;
			} ) ;
		} ) ;
	} ,
	
	onOverlayAdd : function ( ev ) {
		var me = this ;
		var key = me.layer_info.name2key[ev.name] ;
		me.show_layers.push ( key ) ;
		me.show_layers = me.show_layers.sort() ;
		me.updatePermalink() ;
		me.loadLayer ( key ) ;
	} ,
	
	onOverlayRemove : function ( ev ) {
		var me = this ;
		var key = me.layer_info.name2key[ev.name] ;
		me.show_layers = $.grep ( me.show_layers , function(value) { return value != key; });
		me.show_layers = me.show_layers.sort() ;
		me.updatePermalink() ;
	} ,
	

	setMap : function () {
		console.log("CreateMap");
		var me = this ;
		if ( !me.createMap() ) {
			me.map.setView ( [ me.pos.lat , me.pos.lng ] , me.zoom_level ) ;
		}
		me.updateLayers() ;
	} ,
	
	setPositionFromQ : function ( q ) {
		var me = this ;
		var sparql = "#TOOL: WikiShootMe\n" ;
		sparql += 'SELECT ?qc ?qcau { wd:'+q+' wdt:P625 ?qc OPTIONAL { wd:'+q+' wdt:P131 ?au . ?au wdt:P625 ?qcau } }'
		$.get ( me.sparql_url , {
			query:sparql
		} , function ( d ) {
			var found = false ;
			if ( typeof d == 'undefined' || typeof d.results == 'undefined' || typeof d.results.bindings == 'undefined' ) return me.setPositionFromCurrentLocation() ;
			$.each ( d.results.bindings , function ( dummy , v ) {
				var m = v.qc.value.match ( /^Point\((.+?)\s(.+?)\)$/ ) ; // Try item coordinates
				if ( m == null ) m = v.qcac.value.match ( /^Point\((.+?)\s(.+?)\)$/ ) ; // No luck, try admin unit coordinates
				if ( m == null ) return ;
				me.pos = { lat:m[2]*1 , lng:m[1]*1 } ;
                                if (me.zoom_level<12) me.zoom_level=12;
				found = true ;
				return false ;
			} ) ;
			if ( found ) me.setMap() ;
			else me.setPositionFromCurrentLocation() ;
		} , 'json' ) ;
	} ,

	setPositionToMyLocation : function () {
		var me = this ;
		if (me.isIframe()) {
			var r=confirm("To locate yourself you need to open map to full view");
                        if (r==true) {
				window.open(document.location.href, "_top");
			}
		}
		else
		{
			if ( typeof me.marker_me == 'undefined' ) return ; // Paranoia
			me.pos = me.marker_me.getLatLng() ;
                        me.zoom_level=12;
			me.setMap() ;
		}
	} ,
	
	setPositionFromCurrentLocation : function () {
		var me = this ;
		if (navigator.geolocation) {
			me.setMap() ;
			navigator.geolocation.getCurrentPosition ( function (p) {
				me.pos = me.gps2leaflet(p.coords) ;
				me.setMap() ;
			} ) ;
		} else {
			me.setMap() ;
		}
	} ,

        doSearch_sparql : function() {
                var me = this ;
                var query = $('#search_query').val().toString().replace("'", " ");
		$('#search_results_list').html('') ;
                var sparql="SELECT DISTINCT (REPLACE(STR(?q), 'http://www.wikidata.org/entity/', '') as ?title)  ?q ?qLabel ?qDescription WHERE {\n";
                sparql+="  {\n";
                sparql+="  SELECT ?q WHERE {\n";
                sparql+="  hint:Query hint:optimizer 'None'.\n";
                sparql+="  SERVICE wikibase:mwapi {\n";
                sparql+="      bd:serviceParam wikibase:endpoint 'www.wikidata.org';\n";
                sparql+="                      wikibase:api 'Search';\n";
                sparql+="                      mwapi:srsearch '"+ query +" linksto:\"Q33\" linksto:\"P:P625\" ';\n";
                sparql+="                      mwapi:srlimit 200 .\n";
                sparql+="      ?title wikibase:apiOutput mwapi:title.\n";
                sparql+="  }\n";
                sparql+="  BIND(URI(CONCAT(\"http://www.wikidata.org/entity/\", ?title)) as ?q) .\n";
                sparql+="  {\n";
                sparql+="  {\n";
                sparql+="     ?q wdt:P1435 ?p1435 .\n";
                sparql+="     ?q wdt:P17 wd:Q33 .\n";
                sparql+="     ?q wdt:P625 ?coord.\n";
                sparql+="  }\n";
                sparql+="  UNION\n";
                sparql+="  {\n";
                sparql+="     ?q wdt:P361 ?mainitem .\n";
                sparql+="     ?mainitem wdt:P1435 ?p1435 .\n";
                sparql+="     ?mainitem wdt:P17 wd:Q33 .\n";
                sparql+="     ?q wdt:P625 ?coord.\n";
                sparql+="  }\n";
                sparql+="  }\n";
                sparql+="  }\n";
                sparql+="  }\n";
                sparql+="  UNION\n";
                sparql+="  {\n";
                sparql+="      SELECT DISTINCT (?items as ?q) WHERE {\n";
                sparql+="      hint:Query hint:optimizer 'None'.\n";
                sparql+="      VALUES ?placetype { wd:Q856076 wd:Q17468533 }\n";
                sparql+="      ?item wdt:P31 ?placetype .\n";
                sparql+="      ?item wdt:P17 wd:Q33 .\n";
                sparql+="      ?item (rdfs:label|skos:altLabel) ?placename .\n";
                sparql+="      FILTER(STRSTARTS(lcase(?placename), lcase('"+query+"')))\n";
                sparql+="      ?items wdt:P131 ?item . \n";
                sparql+="      {\n";
                sparql+="      {\n";
                sparql+="         ?items wdt:P1435 ?p1435 .\n";
                sparql+="         ?items wdt:P17 wd:Q33 .\n";
                sparql+="      }\n";
                sparql+="      UNION\n";
                sparql+="      {\n";
                sparql+="         ?items wdt:P361 ?mainitem .\n";
                sparql+="         ?mainitem wdt:P1435 ?p1435 .\n";
                sparql+="         ?mainitem wdt:P17 wd:Q33 .\n";
                sparql+="      }\n";
                sparql+="      }\n";
                sparql+="      ?items wdt:P625 ?coord.\n";
                sparql+="    } LIMIT 100\n";
                sparql+="  }\n";
                sparql+="  ?q wdt:P1435 ?p1435 .";
                sparql+="  FILTER (?p1435 NOT IN ( wd:Q31027091, wd:Q65967032, wd:Q19683138 ) )\n";
                sparql+="  FILTER NOT EXISTS {?q wdt:P5008 wd:Q113577693 }\n";
//                sparql+="  FILTER NOT EXISTS {?q wdt:P1435 wd:Q31027091 }\n";
//                sparql+="  FILTER NOT EXISTS {?q wdt:P1435 wd:Q65967032 }\n";
                sparql+="  SERVICE wikibase:label { bd:serviceParam wikibase:language \"[AUTO_LANGUAGE],fi,en,sv,smn,sms,se\". }\n";
                sparql+="} LIMIT 100";

                $.get ( me.sparql_url , {
                        query:sparql
                } , function ( d ) {
                        console.log(JSON.stringify(d));

                        if ( typeof d == 'undefined' || typeof d.results == 'undefined' || typeof d.results.bindings == 'undefined' ) return ;
                        var qs_all = [] ;


                        $.each ( d.results.bindings , function ( dummy , item ) {
                           qs_all.push ( item.title.value )
                        });
			me.wd.getItemBatch ( qs_all , function () {
				var qs = [] ;
				$.each ( qs_all , function ( dummy , q ) {
					var i = me.wd.getItem ( q ) ;
					if ( typeof i == 'undefined' ) return ;
					if ( !i.hasClaims('P625') && !i.hasClaims('P131') ) return ; // No coords or admin unit, useless
					var p31 = i.getClaimItemsForProperty('P31',true) ;
					if ( -1 != $.inArray ( 'Q13406463' , p31 ) ) return ; // Bad instance
					qs.push ( q ) ;
				} ) ;

				var h = '' ;
				$.each ( qs , function ( dummy , q ) {
					h += '<li class="list-group-item sr_result" q="'+q+'" id="sr_'+q+'">' ;
					h += "<div class='sr_title'></div>" ;
					h += "<div class='sr_auto'></div>" ;
					h += "<div class='sr_manual'></div>" ;
					h += '</li>' ;
				} ) ;
			
				$('#search_results_list').html(h) ;
				$('#search_results').show() ;
			
				$('#search_results_list li.sr_result').click ( function () {
					var q = $(this).attr ( 'q' ) ;
					$('#search_dialog').modal ( 'hide' ) ;
					me.setPositionFromQ ( q ) ;
					return false ;
				} ) ;

				$.each ( qs , function ( dummy , q ) {
					wsm_comm.getAutodesc ( {
						q:q,
						lang:me.language,
//						lang:'se',
						mode:'short',
						links:'text',
						format:'json'
					} , function ( d ) {
						var id = 'sr_'+q ;
					        var langcodes = [me.language, "en", "fi", "sv", "se", "smn", "sms"];

						if ( typeof d.result == 'undefined' ) {
							var i=me.wd.getItem ( q );
							var i_label=q;
							var i_description="";
							for (var l = 0; l < langcodes.length; l++) {
								langcode=langcodes[l];
								i_label=i.getLabel(langcode)
								if (i_label!=q) break;
							}
							for (var l = 0; l < langcodes.length; l++) {
								langcode=langcodes[l];
								i_description=i.getDesc(langcode)
								if (i_description!="") break;
							}
							$('#'+id+' div.sr_title').text ( i_label ) ;
							$('#'+id+' div.sr_manual').text ( i_description ) ;
						} else {
							var i_label=d.label;
							if (i_label==q) {
                                                                var i=me.wd.getItem ( q );
								for (var l = 0; l < langcodes.length; l++) {
									langcode=langcodes[l];
									i_label=i.getLabel(langcode);
									if (i_label!=q) break;
								}
							}
							$('#'+id+' div.sr_title').text ( i_label ) ;
							$('#'+id+' div.sr_manual').text ( d.manual_description ) ;
							$('#'+id+' div.sr_auto').text ( d.result ) ;
						}
					} , 'json' ) ;				
				} ) ;
			} ) ;
			
		}, 'json' ) ;
	} ,

	
	doSearch : function () {
		var me = this ;
		var query = $('#search_query').val() ;
		$('#search_results_list').html('') ;

		wsm_comm.searchWikidata ( {
			action:'query',
			list:'search',
			srsearch:query,
			srlimit:25,
			srprop:'',
			format:'json'
		} , function ( d ) {
			var qs_all = [] ;
			$.each ( d.query.search , function ( k , v ) { qs_all.push ( v.title ) } ) ;
			
			me.wd.getItemBatch ( qs_all , function () {
				var qs = [] ;
				$.each ( qs_all , function ( dummy , q ) {
					var i = me.wd.getItem ( q ) ;
					if ( typeof i == 'undefined' ) return ;
					if ( !i.hasClaims('P625') && !i.hasClaims('P131') ) return ; // No coords or admin unit, useless
					var p31 = i.getClaimItemsForProperty('P31',true) ;
					if ( -1 != $.inArray ( 'Q13406463' , p31 ) ) return ; // Bad instance
					qs.push ( q ) ;
				} ) ;
				
				var h = '' ;
				$.each ( qs , function ( dummy , q ) {
					h += '<li class="list-group-item sr_result" q="'+q+'" id="sr_'+q+'">' ;
					h += "<div class='sr_title'></div>" ;
					h += "<div class='sr_auto'></div>" ;
					h += "<div class='sr_manual'></div>" ;
					h += '</li>' ;
				} ) ;
			
				$('#search_results_list').html(h) ;
				$('#search_results').show() ;
			
				$('#search_results_list li.sr_result').click ( function () {
					var q = $(this).attr ( 'q' ) ;
					$('#search_dialog').modal ( 'hide' ) ;
					me.setPositionFromQ ( q ) ;
					return false ;
				} ) ;
			
				$.each ( qs , function ( dummy , q ) {
					wsm_comm.getAutodesc ( {
						q:q,
						lang:me.language,
						mode:'short',
						links:'text',
						format:'json'
					} , function ( d ) {
						var id = 'sr_'+q ;
						if ( typeof d.result == 'undefined' ) {
							var i=me.wd.getItem ( q );
							var langcodes = [me.language, "en", "fi", "sv"];
							var i_label=q;
							var i_description="";
							for (var l = 0; l < langcodes.length; l++) {
								langcode=langcodes[l];
								i_label=i.getLabel(langcode)
								if (i_label!=q) break;
							}
							for (var l = 0; l < langcodes.length; l++) {
								langcode=langcodes[l];
								i_description=i.getDesc(langcode)
								if (i_description!="") break;
							}
							$('#'+id+' div.sr_title').text ( i_label ) ;
							$('#'+id+' div.sr_manual').text ( i_description ) ;
						} else {
							$('#'+id+' div.sr_title').text ( d.label ) ;
							$('#'+id+' div.sr_manual').text ( d.manual_description ) ;
							$('#'+id+' div.sr_auto').text ( d.result ) ;
						}
					} , 'json' ) ;				
				} ) ;
			} ) ;
			
		} ) ;
	} ,
	
	// Checks the administrative unit of the surrounding items, calls back with the first one (ordered by distance) that reaches 5, or ''
	getAdminUnit : function ( lat , lng , callback ) {
		var me = this ;
		var min_cnt = 5 ;

		var p131 = '' ;
		var radius = 2 ;
		var sparql = "#TOOL: WikiShootMe\n" ;
		sparql += 'SELECT ?dist ?unit WHERE { ?place wdt:P131 ?unit .' ;
		sparql += ' SERVICE wikibase:around { ?place wdt:P625 ?location . bd:serviceParam wikibase:center "Point('+lng+' '+lat+')"^^geo:wktLiteral . bd:serviceParam wikibase:radius "'+radius+'" . bd:serviceParam wikibase:distance ?dist } }' ;
		sparql += ' ORDER BY ASC(?dist) LIMIT 50' ;
		$.get ( me.sparql_url , {
			query:sparql
		} , function ( d ) {
			if ( typeof d == 'undefined' || typeof d.results == 'undefined' || typeof d.results.bindings == 'undefined' ) return ;
			var cnt = {} ;
			$.each ( d.results.bindings , function ( k , v ) {
				if ( v.unit.type != 'uri' ) return ;
				var m = v.unit.value.match ( /^.+\/(Q\d+)$/ ) ;
				if ( m == null ) return ;
				var q = m[1] ;
				if ( typeof cnt[q] == 'undefined' ) cnt[q] = 0 ;
				cnt[q]++ ;
				if ( cnt[q] < min_cnt ) return ;
				p131 = q ;
				return false ;
			} ) ;
		} , 'json' ) . always ( function () {
			callback ( p131 ) ;
		} ) ;
	} ,

        restartlayers : function() {
                me.map.eachLayer(function(layer) {
        		if( layer instanceof L.TileLayer )
                		layer.setUrl('https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png?lang=' + me.language);
                	});
                me.cleanLayers();
                me.updateToCurrent() ;
		$('#busy').hide() ;

        },

	init : function () {
		var me = this ;
		me.wd = new WikiData ;
                me.href_target=me.isIframe() ? "_blank" : me.href_target;
		
		if( window.FormData !== undefined ) me.upload_mode = 'upload_background' ;


		var params = me.getHashVars() ;
		if ( params.worldwide == '1' ) me.worldwide = true ;
		if (typeof params.interface_language != 'undefined') me.language=params.interface_language;

		var running = 2 ;
		function fin () {
			running-- ;
			if ( running > 0 ) return ;
			me.tt.addILdropdown ( $('#interface_language_wrapper') ) ;
			
			if ( isMobile() ) {
				// Larger markers for fat thumbs
				$.each ( me.marker_radius , function ( k , v ) {
					me.marker_radius[k] = v * 3 / 2 ;
				} )
			}
			
			if (navigator.geolocation) {
				$('#center_on_me').click ( function() { me.setPositionToMyLocation() } ) ;
			} else {
				$('#center_on_me').hide() ;
			}
			
			$('#update').click ( function () { me.updateLayers() ; } ) ;
			
			
			// Search dialog things
			$('#search').click ( function () {
				$('#search_dialog').modal ( {
				} ) ;
			} ) ;
			$('#search_form').submit ( function (evt) {
				evt.preventDefault();
				me.doSearch_sparql() ;
				return false ;
			} ) ;
			$('#search_dialog').on('shown.bs.modal', function () {
				$('#search_query').focus()
			})


			// Init layers
			$.each ( me.layer_info.key2name , function ( key , name ) {
				me.show_layers.push ( key ) ;
			} ) ;
			me.show_layers = me.show_layers.sort() ;

			me.show_layers = $.grep ( me.show_layers , function(value) { return value != 'flickr' && value != 'mixnmatch' ; }); // Do not show Flickr or Mix'n'match layer by default
			if ( wsm_comm.is_app ) { // Only WIkidata by default for app
				me.show_layers = $.grep ( me.show_layers , function(value) { return value != 'wikipedia' && value != 'commons'; });
			}
			me.show_layers = me.show_layers.sort() ;
			me.full_layers = me.show_layers.join(',') ;
			
			
			// Check URL parameters
			var rewrite_v2_parameters = { // old:new
                                 
				lon:'lng',
				item:'q',
				lang:'interface_language',
				language:'interface_language'
			} ;
			$.each ( rewrite_v2_parameters , function ( k , v ) {
				if ( typeof params[k] != 'undefined' && typeof params[v] == 'undefined' ) params[v] = params[k] ;
			} ) ;

			if ( typeof params.layers != 'undefined' ) {
				me.show_layers = params.layers.replace(/ /g,'_').split(',') ;
				if ( me.show_layers.length == 1 && me.show_layers[0] == '' ) me.show_layers = [] ;
				me.show_layers = me.show_layers.sort() ;
			}

                        if ( typeof params.geojson != 'undefined' ) me.geojson_file = params.geojson.replace(/ /g, "_");  
                        if ( typeof params.geojson_center != 'undefined' && params.geojson_center == 1  ) me.geojson_center = 1;  
			if ( typeof params.zoom != 'undefined' ) me.zoom_level = params.zoom*1 ;
			if ( typeof params.sparql_filter != 'undefined' ) me.sparql_filter = params.sparql_filter ;
			if ( typeof params.lat != 'undefined' && typeof params.lng != 'undefined' ) { // Set location from lat/lng
				me.pos.lat = params.lat*1 ;
				me.pos.lng = params.lng*1 ;
				console.log("lang: " + me.language);

				me.setMap() ;
				me.addMarkerMe() ;
			} else if ( typeof params.q != 'undefined' && params.q.match(/^Q\d+$/) ) { // Set location from item parameter
				var q = params.q ;
				me.setPositionFromQ ( q ) ;
			} else {
				me.setPositionFromCurrentLocation() ;
			}
		}
		
		// SPARQL filter
		$('#sparql_filter_button').click ( function () {
			$('#sparql_filter_dialog').modal ( 'show' ) ;
			$('#worldwide').prop('checked', me.worldwide?true:false );
			$('#sparql_filter_p31').val ( '' ) ;
			$('#sparql_filter_query').val ( me.sparql_filter ) ;
			return false ;
		} ) ;
		$('#sparql_simple_form').submit ( function ( evt ) {
			evt.preventDefault();
			var p31 = $('#sparql_filter_p31').val().toUpperCase() ;
			if ( !p31.match ( /^Q\d+$/ ) ) {
				alert ( me.tt.t('bad_q_number') ) ;
				return ;
			}
			var sparql = "?q wdt:P31/wdt:P279* wd:" + p31 ;
			$('#sparql_filter_query').val ( sparql ) ;
			return false ;
		} ) ;
		$('#sparql_filter_use').click ( function () {
			me.sparql_filter = $.trim ( $('#sparql_filter_query').val() ) ;
			me.worldwide = $('#worldwide').is(':checked') && $.trim(me.sparql_filter) != '' ;
			$('#sparql_filter_dialog').modal ( 'hide' ) ;
                        me.cleanLayers();
			me.updateLayers() ;
		} ) ;
		$('#sparql_filter_clear').click ( function () {
			me.sparql_filter = $.trim ( '' ) ;
			$('#sparql_filter_dialog').modal ( 'hide' ) ;
                        me.cleanLayers();
			me.updateLayers() ;
		} ) ;
		
		// Load user status
//		wsm_comm.checkUserStatus ( function () { fin() } ) ;
		fin();
		
		// Load translation
		me.tt = new ToolTranslation ( { tool: 'wikishootme' , language:me.language, fallback:'en' , callback : function () {
			fin() ;
		} , onLanguageChange : function ( new_language ) {
			me.language = new_language ;
		        var waitlanguageload = function() {
				console.log(me.tt.t('upload_file'));
				if (typeof me.tt.t('upload_file')=="undefined") setTimeout(waitlanguageload, 500);
				else {
					 me.map.eachLayer(function(layer) {
                        		if( layer instanceof L.TileLayer )
                                		layer.setUrl('https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png?lang=' + me.language);
                        		});
                			me.cleanLayers();
                			me.updateToCurrent();
                			$('#busy').hide();
				}
        		};

                        waitlanguageload();
/*                        me.map.eachLayer(function(layer) {
			setTimeout(function() {
                            if( layer instanceof L.TileLayer )
                                layer.setUrl('https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png?lang=' + me.language);
                            });
                            me.cleanLayers();
			    me.updateToCurrent() ;
			}, 4000);
*/
		} } ) ;

	}

} ;
